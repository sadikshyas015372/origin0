from config import *
from models.dbmodel import Table
import io
import xlwt

######## view functions --> have forms

@app.route('/')
def index():
    title = 'Home'
    return render_template('home.html',title=title)

@app.route('/add',methods=['GET','POST'])
def add_pup():
    title = 'Add'
    form = AddForm()
    if form.validate_on_submit():  #if clicked submit button
        item = form.item.data
        price = form.price.data
        quantity = form.quantity.data
        total = price*quantity
        new_pup = Table(item,price,quantity,total)
        db.session.add(new_pup)
        db.session.commit()

        return redirect(url_for('list_pup'))

    return render_template('add.html',form=form,title=title)

@app.route('/list')
def list_pup():
    title = 'List of Products'
    productlist = Table.query.all()
    num = len(productlist)
    return render_template('list.html',productlist=productlist,num = num,title=title)

searchby = []
@app.route('/searchby',methods=['GET','POST'])
def search():
    title = 'search'
    form = SearchForm()
    # form1 = ExportPdf()
    if form.validate_on_submit():
        search = form.search.data
        search = search.strip()
        searchby.clear()
        searchby.append(search)
        name = Table.query.filter((Table.item.ilike(f'%{search}%')))
        return render_template('search_result.html',form=form,name=name)
    
    return render_template('search.html',form=form,title=title)

@app.route('/pdfd' ,methods=['GET','POST'])
def pdf():
    # form = SearchForm()
    productlist = Table.query.all()
    num = len(productlist)
    rendered = render_template("total_sales_pdf.html",productlist=productlist,num=num)
    pdf =pdfkit.from_string(rendered,False)
    response = make_response(pdf)
    response.headers['Content-Type'] = "application/pdf"
    response.headers['Content-Disposition'] = "inline,filename=output.pdf"
    return response
    
@app.route('/pdfdownload' ,methods=['GET','POST'])
def pdf_searched():
    # form = SearchForm()
    name = Table.query.filter((Table.item.ilike(f'%{searchby[0]}%')))
    # num = len(name)
    rendered = render_template("searched_pdf.html",name=name)
    pdf =pdfkit.from_string(rendered,False)
    response = make_response(pdf)
    response.headers['Content-Type'] = "application/pdf"
    response.headers['Content-Disposition'] = "inline,filename=output.pdf"
    return response

@app.route('/excelreport',methods=['GET','POST'])
def excel():
    productlist = Table.query.all()
    output = io.BytesIO()  # output in bytes
    workbook = xlwt.Workbook() # create workbook object
    wb = workbook.add_sheet('Sales Report') #add a sheet 
    #add headers to excel
    #sh.write(0, 0, 'Id')
    wb.write(0, 0, 'item')
    wb.write(0, 1, 'quantity')
    wb.write(0, 2, 'price')
    wb.write(0, 3, 'total')

    i = 0
    for row in productlist:
        #sh.write(idx+1, 0, str(row['id']))
        wb.write(i+1, 0, row.item)
        wb.write(i+1, 1, row.quantity)
        wb.write(i+1, 2, row.price)
        wb.write(i+1, 3, row.total)
        i += 1
    
    workbook.save(output)
    output.seek(0)
   
    return Response(output, mimetype="application/ms-excel", headers={"Content-Disposition":"attachment;filename=sales_report.xls"})
 
@app.route('/excelreport/download',methods=['GET','POST'])
def excelsearch():
    productlist = Table.query.filter((Table.item.ilike(f'%{searchby[0]}%')))
    output = io.BytesIO()  # output in bytes
    workbook = xlwt.Workbook() # create workbook object
    wb = workbook.add_sheet('Sales Report') #add a sheet 
    #add headers
    #sh.write(0, 0, 'Id')
    wb.write(0, 0, 'item')
    wb.write(0, 1, 'quantity')
    wb.write(0, 2, 'price')
    wb.write(0, 3, 'total')

    i = 0
    for row in productlist:
        #sh.write(idx+1, 0, str(row['id']))
        wb.write(i+1, 0, row.item)
        wb.write(i+1, 1, row.quantity)
        wb.write(i+1, 2, row.price)
        wb.write(i+1, 3, row.total)
        i += 1
    
    workbook.save(output)
    output.seek(0)
   
    return Response(output, mimetype="application/ms-excel", headers={"Content-Disposition":"attachment;filename=sales_report.xls"})
 




    

        







