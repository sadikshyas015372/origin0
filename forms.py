# forms.py
from flask_wtf import FlaskForm
from wtforms import StringField,IntegerField,SubmitField

class AddForm(FlaskForm):
    item = StringField('Name of item:')
    price = IntegerField('price per unit:')
    quantity = IntegerField('quantity:')
    ###total = IntegerField('Total:')
    submit = SubmitField('Add to Database')

class SearchForm(FlaskForm):
    search = StringField('Search:')
    submit = SubmitField('Go')

class ExportPdf(FlaskForm):
    pdfsubmit = SubmitField("export to pdf")

